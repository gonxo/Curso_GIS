var popup = null;
var iconImage = './imagenes/rojo.png';
var size = new OpenLayers.Size(30, 40);
var size1 = new OpenLayers.Size(30, 40);
var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
var proyeccion = new OpenLayers.Projection("EPSG:900913"); //EPSG:900913
var proyeccion2 = new OpenLayers.Projection("EPSG:4326");
var controlNavegacion = new OpenLayers.Control.Navigation();
var controlZoom = new OpenLayers.Control.PanZoomBar(); // Zoom
var propiedades = {
    projection: proyeccion,
    displayProjection: proyeccion2,
    units: 'm',
    controls: [controlNavegacion, controlZoom],
    numZoomLevels: 20
};
var map = new OpenLayers.Map("mapa", propiedades);
var init = function () {


    var host = "http://192.168.56.5";
    OpenLayers.ProxyHost = "/cgi-bin/proxy.cgi?url=";

    var layerGooglePhysical = new OpenLayers.Layer.Google("Google Physical", {type: google.maps.MapTypeId.PHYSICAL});
    var layerGoogleSatelital = new OpenLayers.Layer.Google("Google Satelital", {type: google.maps.MapTypeId.SATELLITE});
    var layerGoogleHibrido = new OpenLayers.Layer.Google("Google Hidrido", {type: google.maps.MapTypeId.HYBRID});
    var layerGoogleTerrain = new OpenLayers.Layer.Google("Google Terrain", {type: google.maps.MapTypeId.TERRAIN});

    map.addLayers([layerGooglePhysical, layerGoogleHibrido, layerGoogleSatelital, layerGoogleTerrain]);
    var layerOSM = new OpenLayers.Layer.OSM();
    map.addLayer(layerOSM);

    var controlCapas = new OpenLayers.Control.LayerSwitcher();
    map.addControl(controlCapas);

    var controlCursor = new OpenLayers.Control.MousePosition();
    map.addControl(controlCursor);

    var nombreWMS = "L�mites departamentales";
    var urlWMS = "http://geo.gob.bo/geoserver/wms";
    var propiedadesWMS = {
        layers: 'fondos:departamento1',
        transparent: true
    };

    var layerDepartamentosWMS = new OpenLayers.Layer.WMS(nombreWMS, urlWMS, propiedadesWMS);
    layerDepartamentosWMS.setVisibility(false);
    map.addLayer(layerDepartamentosWMS);

    var layerEolico = new OpenLayers.Layer.WMS(
            "Mapa E�lico",
            host + ":8080/geoserver/wms",
            {
                layers: 'curso-gis:EOLICO',
                transparent: true
            }
    );
    map.addLayer(layerEolico);
    layerEolico.setVisibility(false);

    // Layer WMS mapa Eolico desde PostGIS
    var layerEolicoPG = new OpenLayers.Layer.WMS(
            "Mapa E�lico PostGIS",
            host + ":8080/geoserver/wms",
            {
                layers: 'curso-gis:tbl_eolico',
                transparent: true
            }
    );
    map.addLayer(layerEolicoPG);
    layerEolicoPG.setVisibility(false);

    // Capa Presas 2010  
    var layerPresas = new OpenLayers.Layer.WMS(
            "Mapa de Represas",
            host + ":8080/geoserver/wms",
            {
                layers: 'curso-gis:Presas2010',
                transparent: true
            }
    );
    map.addLayer(layerPresas);
    layerPresas.setVisibility(false);

    var layerJson = new OpenLayers.Layer.Vector(
            "Capa Uyuni-json",
            {
                projection: new OpenLayers.Projection("EPSG:4326"),
                strategies: [new OpenLayers.Strategy.Fixed()],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: './salar_uyuni.json',
                    format: new OpenLayers.Format.GeoJSON()
                })
            }
    );
    map.addLayer(layerJson);
    layerJson.setVisibility(false);

    var controlInfo = new OpenLayers.Control.WMSGetFeatureInfo({
        queryVisible: true,
        eventListeners: {
            getfeatureinfo: function (event) {
                var popup = new OpenLayers.Popup.FramedCloud(
                        'Popup',
                        map.getLonLatFromPixel(event.xy),
                        null,
                        event.text,
                        null,
                        true
                        );
                map.addPopup(popup);

                var info = document.getElementById("informacion");
                var posicion = map.getLonLatFromPixel(event.xy);
                info.innerHTML = posicion.lon + " | " + posicion.lat;
            }
        }
    });
    map.addControl(controlInfo);
    controlInfo.activate();


    // Configuraci�n para mostrar la regi�n de Bolivia
    // Lon: -64.819336, Lat: -17.379999
    var LonLat = new OpenLayers.LonLat(-64.52449, -16.88239);
    var zoom = 6;
    var LonLatTransformado = LonLat.transform(
            new OpenLayers.Projection("EPSG:4326"),
            map.getProjection() // new OpenLayers.Projection("EPSG:900913")
            );
    map.setCenter(LonLatTransformado, zoom);

    /*var popup = null;
     var iconImage = './imagenes/rojo.png';
     var size = new OpenLayers.Size(30, 30);
     var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);*/
    /*var marker, iconMarker, LonLat, text;
     
     //text = "<div style='font-size:13px;color:BLACK; font-family: Helvetica'><h5> " + "title" + "<br />" + "titletwo" + "<br /> " + "description" + " " + "place" + "</h5></div>";
     iconMarker = new OpenLayers.Icon(iconImage, size, offset);
     LonLat = new OpenLayers.LonLat(-68.12978, -16.50471).transform(new OpenLayers.Projection("EPSG:4326"),
     new OpenLayers.Projection(new OpenLayers.Projection("EPSG:900913")));
     
     marker = new OpenLayers.Marker(LonLat, iconMarker);
     marker.events.register('click', marker, function () {
     this.icon.imageDiv.firstChild.setAttribute('src', './imagenes/amarillo.png');
     // popup  CON ESTA PROPIEDAD MOSTRAMOS UN MENSAJE AL MOMENTO DE HACER CLICK EN LOS MARCADORES 
     popup = new OpenLayers.Popup.FramedCloud("Popup",
     new OpenLayers.LonLat(-68.12978, -16.50471).transform(new OpenLayers.Projection("EPSG:4326"),
     new OpenLayers.Projection("EPSG:900913")), null, text, null, true);
     
     
     
     map.addPopup(popup);
     map.setCenter(LonLat, 15);
     });
     
     var markers = new OpenLayers.Layer.Markers("Curso-GIS", {
     displayInLayerSwitcher: false,
     isBaseLayer: false
     });
     map.addLayer(markers);
     markers.addMarker(marker);*/

};

// Cargamos la funci�n init, para desplegar el mapa
window.onload = init;
var marker = [];
function Capa_Postgres() {
    marker2 = [];
    var iconMarker, LonLat1, text;
    var iconImage = './imagenes/rojo.png';
    $.getJSON('http://localhost:83/curso_gis/Conexion/Query.php?callback=parseResponse?', function (data) {
        for (var geoloc in data) {
            console.log(data);

            var ubicacion = parseInt(data[geoloc]['departamentoid']);
            var tipo = parseInt(data[geoloc]['departamentotipo']);
            var latitude = parseFloat(data[geoloc]['latitud']);
            var longitude = parseFloat(data[geoloc]['longitud']);
            var text = "NOMBRE: ";
            var text1 = "DESCRIPCION:";
            var desc = "<div style='font-size:13px;color:black; font-family: Helvetica'> " + text + data[geoloc]['departamentonombre'] + "<br />" + text1 + data[geoloc]['departamentouniversidad'];
            text = "<div style='font-size:13px;color:BLACK; font-family: Helvetica'><h5> " + "title" + "<br />" + "titletwo" + "<br /> " + "description" + " " + "place" + "</h5></div>";
            iconMarker = new OpenLayers.Icon('./imagenes/departamento.png', size, offset);
            var iconMarker2 = new OpenLayers.Icon('./imagenes/universidad.png', size, offset);
            LonLat1 = new OpenLayers.LonLat(longitude, latitude).transform(new OpenLayers.Projection("EPSG:4326"),
                    new OpenLayers.Projection(new OpenLayers.Projection("EPSG:900913")));
            if (tipo === 2)
            {
                marker[ubicacion] = new OpenLayers.Marker(LonLat1, iconMarker);
                //  marker[ubicacion] = new OpenLayers.Marker(LonLat, iconMarker);
                marker[ubicacion].events.register('click', marker[ubicacion], function () {
                    // this.icon.imageDiv.firstChild.setAttribute('src', './imagenes/amarillo.png');
                    // popup  CON ESTA PROPIEDAD MOSTRAMOS UN MENSAJE AL MOMENTO DE HACER CLICK EN LOS MARCADORES 
                    popup = new OpenLayers.Popup.FramedCloud("Popup",
                            new OpenLayers.LonLat(longitude, latitude).transform(new OpenLayers.Projection("EPSG:4326"),
                            new OpenLayers.Projection("EPSG:900913")), null, desc, null, true);



                    map.addPopup(popup);
                    //map.setCenter(LonLat1, 15);
                });
            }
            else
            {
                marker[ubicacion] = new OpenLayers.Marker(LonLat1, iconMarker2);
                //  marker[ubicacion] = new OpenLayers.Marker(LonLat, iconMarker);
                marker[ubicacion].events.register('click', marker[ubicacion], function () {
                    // this.icon.imageDiv.firstChild.setAttribute('src', './imagenes/amarillo.png');
                    // popup  CON ESTA PROPIEDAD MOSTRAMOS UN MENSAJE AL MOMENTO DE HACER CLICK EN LOS MARCADORES 
                    popup = new OpenLayers.Popup.FramedCloud("Popup",
                            new OpenLayers.LonLat(longitude, latitude).transform(new OpenLayers.Projection("EPSG:4326"),
                            new OpenLayers.Projection("EPSG:900913")), null, desc, null, true);



                    map.addPopup(popup);
                    //map.setCenter(LonLat1, 15);
                });
            }


            var markers = new OpenLayers.Layer.Markers("Capa_Bolivia", {
                displayInLayerSwitcher: false,
                isBaseLayer: false

            });
            markers.addMarker(marker[ubicacion]);
                map.addLayer(markers);
                  


        }



    });
}
Capa_Postgres();



