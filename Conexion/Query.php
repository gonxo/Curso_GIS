<?php
header("access-control-allow-origin: *");
include ("conexion.php");
$queryConexion = "";
$con = connect();
$queryView = array();
$result = pg_query($con,'SELECT departamentoid, departamentogeoloc, departamentonombre, departamentouniversidad, departamentotipo
                         FROM departamento ');
while($row = pg_fetch_array($result)) {
    $ubicacionid = trim ($row['departamentoid']);
    $ubicacionplaca = trim ($row['departamentonombre']);
    $ubicacionmarca = trim ($row['departamentouniversidad']);
    $ubicacioncolor = trim ($row['departamentotipo']);
    $ubicaciongeoloc  = trim($row['departamentogeoloc']);
    $latlon = explode(",", $ubicaciongeoloc);
    $latitud = $latlon[0];
    $longitud  = $latlon[1];
	$queryView [] = array (
        'departamentoid' => $ubicacionid,
        'departamentonombre' => $ubicacionplaca,
        'departamentouniversidad' => $ubicacionmarca,
        'departamentotipo' => $ubicacioncolor,
        'latitud' => $latitud,
        'longitud' => $longitud
    );
}
$queryConexion = json_encode($queryView);
echo $queryConexion;



